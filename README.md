# Conversor de bases
Essa aplicação está sendo desenvolvida pelos alunos da UTFPR câmpus Pato Branco para a matéria de Introdução à engenharia. Sua funcionalidade é converter um número de base decimal em uma base binária.

Alunos:

| Nome | Nickname | Email |
| ---- | -------- | ----- |
| Thomas Delfs             | Thommasdel       | [thomas.delfs2@hotmail.com](mailto:thomas.delfs2@hotmail.com)     
| Natália Gonçalves Ramos  | nataliagramos    | [natflyer1998@gmail.com](mailto:natflyer1998@gmail.com)        



